#ifndef TILEMAP_HPP_
# define TILEMAP_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <list>
#include "../interface/IGame.hpp"
#include <iostream>
#include <map>

#define START(a, size) (((a * size) / 2) + size) // -size
#define START_VERT(a, size, height, level) ((((a * size) / 2) - size) - ((height / 5) * level * 10) - 230)
#define VSTART 60

class TileMap : public sf::Drawable, public sf::Transformable
{
private:
  sf::VertexArray m_vertices;
  sf::Texture m_tileset;
  std::list<struct Entity> ent_buffer;
  sf::SoundBuffer	audio_buffer;
  sf::Sound		sound;
  sf::Music		game_music;
  sf::Music		game_over_music;
  int			anim_timer;
  
public:
  TileMap();
  ~TileMap();

public:
  bool load(const struct Tileset_info &tileset, sf::Vector2u tileSize, const char** tiles,
	    unsigned int width, unsigned int height, int level,
	    const std::list<struct Entity> &entity,
	    const std::map<char, TileInfo> &tilesInfo, int timer, int framerate);
  void	setRegularQuad(sf::Vertex *quad, int i,  int j, int width, sf::Vector2u tileSize,
		       int level);
  void	setAnimatedQuadUp(sf::Vertex *quad, int timer, int framerate, int i,  int j, int width,
			sf::Vector2u tileSize, int level);
  void	setAnimatedQuadDown(sf::Vertex *quad, int timer, int framerate, int i,  int j, int width,
			sf::Vector2u tileSize, int level);
  void	setAnimatedQuadLeft(sf::Vertex *quad, int timer, int framerate, int i,  int j, int width,
			sf::Vector2u tileSize, int level);
  void	setAnimatedQuadRight(sf::Vertex *quad, int timer, int framerate, int i,  int j, int width,
			sf::Vector2u tileSize, int level);

private:
  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif
