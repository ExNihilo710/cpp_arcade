#include "../interface/IGraphic.hpp"
#include "TileMap.hpp"
#include "../protocol/Protocol.hpp"

#ifndef ISO_HPP_
#define ISO_HPP_

#define VOID 0
#define BLOCK 1
#define GROUND 2
#define SVOID 99
#define TILE_X 65
#define TILE_Y 57
#define MAX_X 28
#define MAX_Y 32

class	ExiiLib : public IGraphic{

private:
  sf::RenderWindow	window;
  sf::Event		event;
  TileMap		map;
  TileMap		map2;
  int			max_x;
  int			max_y;
  char			**sub_layer;
  std::string		tileset_path;
  std::string		lib_name;
  int			timer;
  sf::Font		font;
  sf::Text		sftext;
  sf::Texture		background_img;
  sf::Sprite		background_spt;
  
public:
  ExiiLib(const struct Tileset_info &tileset, int ,int );
  ~ExiiLib();

public:
  const std::string	&getGLibName(void) const;
  void	printText(const std::string &ressource, const std::string &text,
			  const Position &whereOnScreen);
  void	setScreen(const std::string &, const Tileset_info &, const char **map,
		  const std::map<char, TileInfo> &tilesInfos,
		  const std::list<struct Entity> &entitys, int frame, int operation);

  void	fillInput(Input &in);
  void	keyUp(sf::Event event, Input &in);
  void	keyLeft(sf::Event event, Input &in);
  void	keyRight(sf::Event event, Input &in);
  void	keyDown(sf::Event event, Input &in);
  void	keyTwo(sf::Event event, Input &in);
  void	keyThree(sf::Event event, Input &in);
  void	keyFour(sf::Event event, Input &in);
  void	keyFive(sf::Event event, Input &in);
  void	keyEight(sf::Event event, Input &in);
  void	keyNine(sf::Event event, Input &in);
  void	keyEnter(sf::Event event, Input &in);
  void	keySpace(sf::Event event, Input &in);
  const char	**generateSubLayer(const char **tmp);
  void	refresh(void);
};

extern "C"
{
  IGraphic *newGraphicObject(const struct Tileset_info &ts, int xws, int yws)
  {
    return new ExiiLib(ts, xws, yws);
  }
}

#endif 
