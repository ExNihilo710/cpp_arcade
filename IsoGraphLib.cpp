#include "IsoGraphLib.hpp"

# define FRAME_X 50
# define FRAME_Y 50

ExiiLib::ExiiLib(const struct Tileset_info &tileset, int wsx, int wsy){
  lib_name = "SFML 2.3.2";
  window.create(sf::VideoMode(1920, 1080), "Arcade", sf::Style::Default);
  max_x = wsx;
  max_y = wsy;
  sub_layer = NULL;
  if(!font.loadFromFile("rsc/Wild West Pixel.ttf"))
    std::cerr << "Font loading failed" << std::endl;
  sftext.setCharacterSize(24);
  sftext.setColor(sf::Color::Yellow);
}

ExiiLib::~ExiiLib(){

}

const std::string	&ExiiLib::getGLibName(void) const{
  return lib_name;
}

void	ExiiLib::printText(const std::string &ressource, const std::string &text,
		  const Position &whereOnScreen){
  if(ressource != "" )
    if(!font.loadFromFile(ressource.c_str()))
      std::cerr << "Font loading failed" << std::endl;
  sftext.setFont(font);
  sftext.setString(text.c_str());
  sftext.setPosition(whereOnScreen.x, whereOnScreen.y);
  window.draw(sftext);
}

const char	**ExiiLib::generateSubLayer(const char **tmp){
  int	i = 0;
  int	j;

  if(sub_layer == NULL){
    sub_layer = new char*[max_y];
    while(i < max_y){
      j = 0;
      sub_layer[i] = new char[max_x];
      while(j < max_x){
	if(tmp[i][j] == BLOCK)
	  sub_layer[i][j] = BLOCK;
	else if(tmp[i][j] == VOID)
	  sub_layer[i][j] = GROUND;
	else if(tmp[i][j] == SVOID)
	  sub_layer[i][j] = VOID;
	else
	  sub_layer[i][j] = GROUND;
	j++;
      }
      i++;
    }
  }
  return const_cast<const char**>(sub_layer);
}

void	ExiiLib::setScreen(const std::string &img, const struct Tileset_info &ressource,
			   const char **tile_map,
			   const std::map<char, struct TileInfo> &tilesInfos,
			   const std::list<struct Entity> &entity, int frame, int operation){
  if(window.isOpen()){
    window.clear();
    if(timer)
      timer--;
    else
      timer = FRAMEPERSECOND;
    if(operation == TILE){
      if(ressource.texture_3diso == ""){
	if(!this->map.load(ressource, sf::Vector2u(TILE_X, TILE_Y), tile_map, max_x, max_y, 1, entity, tilesInfos, frame, FRAMEPERSECOND))
	  return ;
	if(!this->map2.load(ressource, sf::Vector2u(TILE_X, TILE_Y), generateSubLayer(tile_map), max_x, max_y, 0, entity, tilesInfos, frame, FRAMEPERSECOND))
	  return ;
      }
      else{
	if(!this->map.load(ressource, sf::Vector2u(TILE_X, TILE_Y), tile_map, max_x, max_y, 1, entity, tilesInfos, frame, FRAMEPERSECOND))
	  return ;
	if(!this->map2.load(ressource, sf::Vector2u(TILE_X, TILE_Y), generateSubLayer(tile_map), max_x, max_y, 0, entity, tilesInfos, frame, FRAMEPERSECOND))
	  return ;
      }
      window.draw(map2);
      window.draw(map);
      window.draw(sftext);
    }
    else if(operation == IMAGE){
      if(!background_img.loadFromFile(img)){
	std::cerr << "Fail to load image" << std::endl;
	return ;
      }
      background_spt.setTexture(background_img);
      window.draw(background_spt);
    }
  }
}

void	ExiiLib::keyUp(sf::Event event, Input &in){
    switch(event.type)
      {
      case sf::Event::KeyPressed:
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	  in.up = true;
	break;
      case sf::Event::JoystickMoved:
	if(sf::Joystick::getAxisPosition(0, sf::Joystick::Y) < 0)
	  in.up = true;
	break;
      }
}

void	ExiiLib::keyLeft(sf::Event event, Input &in){
    switch(event.type)
      {
      case sf::Event::KeyPressed:
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	  in.left = true;
	break;
      case sf::Event::JoystickMoved:
	if(sf::Joystick::getAxisPosition(0, sf::Joystick::X) < 0)
	  in.left = true;
	break;
      }
}

void	ExiiLib::keyRight(sf::Event event, Input &in){
    switch(event.type)
      {
      case sf::Event::KeyPressed:
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	  in.right = true;
	break;
      case sf::Event::JoystickMoved:
	if(sf::Joystick::getAxisPosition(0, sf::Joystick::X) > 0)
	  in.right = true;
	break;
      }
}

void	ExiiLib::keyDown(sf::Event event, Input &in){
    switch(event.type)
      {
      case sf::Event::KeyPressed:
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	  in.down = true;
	break;
      case sf::Event::JoystickMoved:
	if(sf::Joystick::getAxisPosition(0, sf::Joystick::Y) > 0)
	in.down = true;
      break;
    }
}

void	ExiiLib::keyTwo(sf::Event event, Input &in){
    switch(event.type)
      {
      case sf::Event::KeyPressed:
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
	  in.num2 = true;
	break;
    }
}

void	ExiiLib::keyThree(sf::Event event, Input &in){
  switch(event.type)
    {
    case sf::Event::KeyPressed:
      if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
	in.num3 = true;
      break;
    }
}

void	ExiiLib::keyFour(sf::Event event, Input &in){
  switch(event.type)
    {
    case sf::Event::KeyPressed:
      if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
	in.num4 = true;
      break;
    }
}

void	ExiiLib::keyFive(sf::Event event, Input &in){
  switch(event.type)
    {
    case sf::Event::KeyPressed:
      if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num5))
	in.num5 = true;
      break;
    }
}

void	ExiiLib::keyEight(sf::Event event, Input &in){
  switch(event.type)
    {
    case sf::Event::KeyPressed:
      if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num8))
	in.num8 = true;
      break;
    }
}

void	ExiiLib::keyNine(sf::Event event, Input &in){
  switch(event.type)
    {
    case sf::Event::KeyPressed:
      if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num9))
	in.num9 = true;
      break;
    }
}

void	ExiiLib::keyEnter(sf::Event event, Input &in){
  switch(event.type)
    {
    case sf::Event::KeyPressed:
      if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
	in.enter = true;
      break;
    }
}

void	ExiiLib::keySpace(sf::Event event, Input &in){
  switch(event.type)
    {
    case sf::Event::KeyPressed:
      if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	in.space = true;
      break;
    }
}

void	ExiiLib::fillInput(Input &in){
  while(window.pollEvent(event)){
    switch(event.type)
      {
      case sf::Event::Closed:
	window.close();
	in.escape = true;
	break;
      case sf::Event::KeyPressed:
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)){
	  window.close();
	  in.escape = true;
	}
	else{
	  keyUp(event, in);
	  keyLeft(event, in);
	  keyRight(event, in);
	  keyDown(event, in);
	  keyTwo(event, in);
	  keyThree(event, in);
	  keyFour(event, in);
	  keyFive(event, in);
	  keyEight(event, in);
	  keyNine(event, in);
	  keyEnter(event, in);
	  keySpace(event, in);
	}
	break;
      default:
	break;
      }
  }
}

void	ExiiLib::refresh(void){
  window.display();
}
