#include "TileMap.hpp"


#define ANIM 10

TileMap::TileMap(){
  anim_timer = 1;
  if(!game_music.openFromFile("rsc/18-Battle.wav"))
    std::cerr << "Audio file loading failed" << std::endl;
  if(!game_over_music.openFromFile("rsc/22-Game Over.wav"))
    std::cerr << "Audio file loading failed" << std::endl;
  game_music.play();
}

TileMap::~TileMap(){

}

bool TileMap::load(const struct Tileset_info &tileset, sf::Vector2u tileSize, const char** tiles,
		   unsigned int width, unsigned int height, int level,
		   const std::list<struct Entity> &entity,
		   const std::map<char, TileInfo> &tilesInfo,
		   int timer, int framerate)
  {
    std::list<struct Entity>::const_iterator	it;
    int						ite;
    std::list<struct Entity>::const_iterator	ext_it;
    
    if (ent_buffer.size() == 0){
      ent_buffer = entity;
    }
    if (!m_tileset.loadFromFile(tileset.texture_3diso))
      return false;
    
    m_vertices.setPrimitiveType(sf::Quads);
    m_vertices.resize(width * height * 4);
    
    for (unsigned int i = 0; i < height; ++i)
      for (unsigned int j = 0; j < width; ++j)
	{
	  int tileNumber = tilesInfo.at(tiles[i][j]).x;
	  int tu = tileNumber % (m_tileset.getSize().x / tileSize.x);
	  int tv = tileNumber / (m_tileset.getSize().x / tileSize.x);
	  
	  sf::Vertex* quad = &m_vertices[(j + i * width) * 4];
	  if(!tilesInfo.at(tiles[i][j]).animation)
	    setRegularQuad(quad, i, j, width, tileSize, level);
	  
	  else{
	    it = ent_buffer.begin();
	    ite = 0;
	    ext_it = entity.begin();
	    while(it != ent_buffer.end() && ext_it != entity.end()){
	      if(it->tile == (int)tiles[i][j])
	      	break;
	      it++;
	      ext_it++;
	      ite++;
	    }
	    
	    if((ext_it->x != it->x) || (ext_it->y != it->y) || (anim_timer > 0)){
	      if(anim_timer == 0)
		anim_timer = 10 * entity.size();
	      if(anim_timer == 1)
		ent_buffer = entity;
	      if(ext_it->x - it->x < 0)
		setAnimatedQuadLeft(quad, timer, framerate, it->y, it->x, width, tileSize, level);
	      else if(ext_it->x - it->x > 0)
		setAnimatedQuadRight(quad, timer, framerate, it->y, it->x, width, tileSize, level);
	      else if(ext_it->y - it->y < 0){
		setAnimatedQuadUp(quad, timer, framerate, it->y, it->x, width, tileSize, level);
		setRegularQuad(quad, i, j, width, tileSize, level);
	      }
	      else if(ext_it->y - it->y > 0){
		setAnimatedQuadDown(quad, timer, framerate, it->y, it->x, width, tileSize, level);
		setRegularQuad(quad, i, j, width, tileSize, level);
	      }
	      anim_timer--;
	    }
	    else
	      setRegularQuad(quad, i, j, width, tileSize, level);
	  }
	  quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv * tileSize.y);
	  quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
	  quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
	  quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);
	}
    return true;
  }

void	TileMap::setRegularQuad(sf::Vertex *quad, int i,  int j, int width, sf::Vector2u tileSize, int level){
  quad[0].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + START(width, tileSize.x),
				  (j + i) * ((tileSize.y / 3) - 1) + (START(width, tileSize.x) /9) - level * ((tileSize.y / 3)) - VSTART);
  
  quad[1].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + (tileSize.x - 1) + START(width, tileSize.x),
				  (j + i) * ((tileSize.y / 3) - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - VSTART);
  
  quad[2].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + (tileSize.x - 1) + START(width, tileSize.x),
				  (j + i) * ((tileSize.y / 3) - 1) + (tileSize.y - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - VSTART);
  
  quad[3].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + START(width, tileSize.x),
				  (j + i) * ((tileSize.y / 3) - 1) + (tileSize.y - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - VSTART);
  
}

void	TileMap::setAnimatedQuadLeft(sf::Vertex *quad, int timer, int framerate, int i,  int j, int width, sf::Vector2u tileSize, int level){
  quad[0].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + START(width, tileSize.x) - (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (START(width, tileSize.x) /9) - level * ((tileSize.y / 3)) - (VSTART + (tileSize.x / (anim_timer))));
  
  quad[1].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + (tileSize.x - 1) + START(width, tileSize.x) - (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART + (tileSize.x / (anim_timer))));
  
  quad[2].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + (tileSize.x - 1) + START(width, tileSize.x) - (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (tileSize.y - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART + (tileSize.x / (anim_timer))));
  
  quad[3].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + START(width, tileSize.x) - (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (tileSize.y - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART + (tileSize.x / (anim_timer))));
}

void	TileMap::setAnimatedQuadRight(sf::Vertex *quad, int timer, int framerate, int i,  int j, int width, sf::Vector2u tileSize, int level){
  quad[0].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + START(width, tileSize.x) + (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (START(width, tileSize.x) /9) - level * ((tileSize.y / 3)) - (VSTART - (tileSize.x / (anim_timer))));
  
  quad[1].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + (tileSize.x - 1) + START(width, tileSize.x) + (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART - (tileSize.x / (anim_timer))));
  
  quad[2].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + (tileSize.x - 1) + START(width, tileSize.x) + (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (tileSize.y - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART - (tileSize.x / (anim_timer))));
  
  quad[3].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + START(width, tileSize.x) + (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (tileSize.y - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART - (tileSize.x / (anim_timer))));
}

void	TileMap::setAnimatedQuadDown(sf::Vertex *quad, int timer, int framerate, int i,  int j, int width, sf::Vector2u tileSize, int level){
  quad[0].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + START(width, tileSize.x) - (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (START(width, tileSize.x) /9) - level * ((tileSize.y / 3)) - (VSTART - (tileSize.x / (anim_timer))));
  
  quad[1].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + (tileSize.x - 1) + START(width, tileSize.x) - (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART - (tileSize.x / (anim_timer))));
  
  quad[2].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + (tileSize.x - 1) + START(width, tileSize.x) - (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (tileSize.y - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART - (tileSize.x / (anim_timer))));
  
  quad[3].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + START(width, tileSize.x) - (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (tileSize.y - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART - (tileSize.x / (anim_timer))));
}

void	TileMap::setAnimatedQuadUp(sf::Vertex *quad, int timer, int framerate, int i,  int j, int width, sf::Vector2u tileSize, int level){
  quad[0].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + START(width, tileSize.x) + (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (START(width, tileSize.x) /9) - level * ((tileSize.y / 3)) - (VSTART + (tileSize.x / (anim_timer))));
  
  quad[1].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + (tileSize.x - 1) + START(width, tileSize.x) + (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART + (tileSize.x / (anim_timer))));
  
  quad[2].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + (tileSize.x - 1) + START(width, tileSize.x) + (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (tileSize.y - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART + (tileSize.x / (anim_timer))));
  
  quad[3].position = sf::Vector2f((j - i) * ((tileSize.x / 2) - 1) + START(width, tileSize.x) + (tileSize.x / (anim_timer)),
				  (j + i) * ((tileSize.y / 3) - 1) + (tileSize.y - 1) + (START(width, tileSize.x)  / 9) - level * ((tileSize.y / 3)) - (VSTART + (tileSize.x / (anim_timer))));
}

void TileMap::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
  states.transform *= getTransform();
  states.texture = &m_tileset;
  target.draw(m_vertices, states);
}
